import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import router from './router';
import store from './store';
import { BASE_URL, CSRF_COOKIE, CSRF_HEADER } from './config';
import './main.scss';

Vue.config.productionTip = false;
axios.defaults.baseURL = BASE_URL;
axios.defaults.withCredentials = true;
axios.defaults.xsrfCookieName = CSRF_COOKIE;
axios.defaults.xsrfHeaderName = CSRF_HEADER;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
