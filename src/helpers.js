// Remove this comment after adding another helper here
// eslint-disable-next-line
export const errorParser = (err) => {
  console.error(err);
  if (!err) return 'Unknown error';
  if (err.response && err.response.data) {
    if (Array.isArray(err.response.data)) return err.response.data.map(errorParser).join('\n');
    if (err.response.data.detail) return errorParser(err.response.data.detail);
    return err.response.data.toString();
  }
  if (typeof err === 'string' || err instanceof String) return err;
  return err.message;
};
