import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';
import measurements from './measurements';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    measurements,
  },
});
