import axios from 'axios';
import { errorParser } from '../helpers';
import { MEASUREMENT_REFRESH_RATE } from '../config';

export default {
  namespaced: true,
  state: {
    latest: null,
    intervalId: null,
  },
  mutations: {
    setLatest(state, latest) {
      state.latest = latest;
    },
    setIntervalId(state, id) {
      state.intervalId = id;
    },
  },
  actions: {
    getLatest({ commit }) {
      return axios.get(
        'api/v1/measurements/latest/',
        { headers: { 'Cache-Control': 'no-cache' } },
      ).then((resp) => {
        commit('setLatest', resp.data);
      }).catch(err => Promise.reject(errorParser(err)));
    },
    startInterval({ state, commit, dispatch }, errorCallback) {
      if (state.intervalId !== null) clearInterval(state.intervalId);
      commit(
        'setIntervalId',
        setInterval(() => {
          errorCallback(null);
          dispatch('getLatest').catch((error) => {
            commit('setLatest', null);
            errorCallback(error);
          });
        }, MEASUREMENT_REFRESH_RATE),
      );
    },
  },
};
