# WattSim

Front-end pour la visualisation des mesures de circuits WattSim

## Setup

```
npm install
```

### Serveur de développement

```
npm run serve
```

### Build de production

```
npm run build
```

### Linting

```
npm run lint
```
